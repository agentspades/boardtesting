global startButton
global testButton
global trap1
global trap2
global trap3
global trap4
global trap5

startButton = "GPIO2"
testButton = "GPIO17"
trap1 = "GPIO5"
trap2 = "GPIO6"
trap3 = "GPIO13"
trap4 = "GPIO19"
trap5 = "GPIO26"


def trap(n):
    if n == 0:
        return trap1
    elif n == 1:
        return trap2
    elif n == 2:
        return trap3
    elif n == 3:
        return trap4
    elif n == 4:
        return trap5
