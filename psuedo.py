import gpiozero
import time
import pins
import logging
import datetime

logging.basicConfig(filename=f'{datetime.datetime.now().date()}.log',
                    encoding='utf-8', level=logging.DEBUG)

# startButton = gpiozero.Button(pins.startButton)
# testButton = gpiozero.LED(pins.testButton)
# trap1 = gpiozero.LED(pins.trap1)
# trap2 = gpiozero.LED(pins.trap2)
# trap3 = gpiozero.LED(pins.trap3)
# trap4 = gpiozero.LED(pins.trap4)
# trap5 = gpiozero.LED(pins.trap5)

fail = False    # set this to true to simulate fail test or false for passing tests
errors = []


def tamper_test():
    print("\nTesting tamper switch")
    time.sleep(1)
    print("Sending signal from pin GPIO17...")
    # the only way i can see to just turn a pin on is to use LED
    # testButton.on()
    print("Signal sent")
    time.sleep(1)
    print("Waiting for message")
    time.sleep(1)
    if(not fail):
        print("Message received correctly")
        return(0)
    else:
        print("Message failed")
        return(1)


def trap_test():
    res = []
    print("\nTesting traps")
    time.sleep(1)
    for trap in range(5):
        print(f"Testing trap {trap+1}")
        print(f"Sending signal from pin {pins.trap(trap)}...")
        # pins.trap(trap).on()
        time.sleep(1)
        print("Signal sent")
        print("Waiting for message")
        time.sleep(1)
        if(not fail):
            print("Message received correctly")
        else:
            print("Message failed")
            res.append(f"Trap: {trap+1} FAILED")
    return res


def runTests(run):
    tamper_test_res = tamper_test()
    if(tamper_test_res != 0):
        errors.append(f"Run: {run+1}\tTamper error")
    trap_test_res = trap_test()
    if (len(trap_test_res) > 0):
        errors.append(f"Run: {run+1}\t{trap_test_res}")
    return errors


def main():
    print(f"You have pressed the start button connected to {pins.startButton}")
    time.sleep(2)
    print("Starting Testing...")
    runs = 0
    failed = 0
    passed = 0
    errors = []
    while(runs < 2):
        errors = runTests(runs)
        if len(errors) == 0:
            passed += 1
        else:
            failed += 1
        runs += 1
    logging.info(f"Tests passed: {passed}")
    logging.info(f"Tests failed: {failed}")
    logging.info(f"Total tests run: {passed+failed}")

    print(f"\n\nTests passed: {passed}")
    print(f"Tests failed: {failed}")
    print(f"Total tests run: {passed+failed}")
    if (failed > 0):
        print("\n\nTEST FAILED!\n")
        logging.info("TEST FAILED")
        print("Errors:")
        for error in errors:
            print(error)
            logging.info(error)
        exit(1)
    else:
        print("\n\nTEST PASSED!\n")
        logging.info("TEST PASSED")
        exit(0)


main()


# startButton.when_pressed = main
